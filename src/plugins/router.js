import {createWebHistory, createRouter}  from "vue-router";



const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component:()=> import('../pages/Home'),
        },
        {
            path: '/post',
            component:()=> import('../pages/Post')
        },
        {
            path: '/user',
            component:()=> import('../pages/Users')
        },
        {
            path: '/course',
            component:()=> import('../pages/Course')
        },
        {
            path: '/setting',
            component:()=> import('../pages/Setting')
        },
        {
            path: '/teacher',
            component:()=> import('../pages/Teacher')
        },
        {
            path: '/test',
            component:()=> import('../pages/Test')
        },
        {
            path: '/dtm',
            component: () => import('../pages/Dtm')
        },
        {
            path: '/variant',
            component: () => import('../pages/TestVariant')
        },
        {
            path: '/pupils',
            component:() => import('../pages/Pupils')
        },
        {
            path: '/course/id',
            component:() => import('../pages/CourseData')
        }
       
    ],
  });

export default router;